####################################################
#  Set default configuration for all users
####################################################

# Editor, browser and terminal preferences
config general.source_editor -set /usr/bin/gvim
config runner.rerun.rerun_vsif_editor -set gvim
config general.Browser -set firefox
config general.open_terminal_cmd -set gnome-terminal 

# DRM preferences
config runner.drm.enable_new_drm_job_manager -set true
config runner.drm.drm -set PARALLEL_LOCAL
config runner.drm.use_drm_job_arrays -set false 
config runner.support_runner_64bit_seed_generation -set true

# Misc preferences
config session.refresh.auto_refresh_delta -set 120 

# Post session automation preferences
config automation.batch_commands.auto_execute_batch -set true
config automation.batch_commands.auto_execute_batch_order -set  AFTER_PREDEFINED_AUTOMATIONS
config automation.batch_commands.auto_execute_batch_script -set $env(DRINK_HOME)/../vmanager_config/post_session_automation.tcl
config automation.batch_commands.auto_execute_batch_type -set SCRIPT

