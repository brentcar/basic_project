proc execute {attributes} {
   upvar attributes attrs
   #  Build vAPI command
   set vm_root [exec vm_root]
   set python3 [eval exec ls [glob [format "%s/%s" $vm_root "/tools/python*/bin/python3"]]]

   set args [format "%s/basic_project/vmanager_config/vAPI_scripts/export_sessions.py" $::env(HOME)]

   set cmd [list $python3 $args]
   puts $cmd
   set response [eval exec $cmd]
   puts "======================================================"
   puts $response
   puts "======================================================"

}
