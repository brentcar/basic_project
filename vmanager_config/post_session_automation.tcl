puts "=========================================================="
puts "             Starting post session automation"
puts "=========================================================="

#  Show sessions and vPlan loaded
show

#  Generate post session summary report

# Load vPlan for report
load -vplan $env(DRINK_HOME)/basic_vplan.vplanx
set session_view Default_Project_View

report_summary -out $env(DRINK_HOME)/summary_report.html -overwrite -title "Latest Summary Report" -vplan -tests -metrics -sessions 
