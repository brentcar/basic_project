###############################################################################
#########################  add_export_button.py  ##############################
###############################################################################
#
# Adds export button to vManager GUI allowing for_credit sessions to be easily
#   exported to be shared for master vManager session
#
#
###############################################################################
#enable/disable 'debug'
debug = 0

import json
import os

import utility_lib
from utility_lib import *

import filter_lib
from filter_lib import *

#  Import Python date library
#    used to convert vManager epoch time
from datetime import datetime


#  Check response from vAPI commands and abort if vAPI endpoint fails
def check_response (url="", response=None, exit_status=1, debug=0) :
    global pflag
    if response == None :
      print("\nvAPI request: " + url + " failed! \n")
      sys.exit(exit_status)
    #  If not a passing status_code, print status_code, reason and exit with requested exit_status
    if response.status_code not in [200, 204, 206] :
      if "already exists" in response.text :
         print(response.text)
         pflag = 1         
      else:
         print("\nvAPI request: " + url + " failed with exit code: " + str(response.status_code) +  " !")
         print("Failure reason: " + response.text + "\n" )
         sys.exit(exit_status)

    if debug == 1 :
      try :
          foo = response.json()
      except :
         #  Not a json response
         print(response)
         sys.exit(exit_status)
      else :
         #  We do have a json response
         response_list = response.json()
         print(json.dumps(response_list, indent=4, sort_keys=True,separators = (",",":")))    


vmgr_server = "localhost:8080"
vmgr_project = "vmgr"
vmgr_username = os.environ["USER"]
vmgr_password = "letmein"


# initial connection to the server to get list of projects using base vmgr project
set_server(project=vmgr_project,server=vmgr_server,user=vmgr_username,passwd=vmgr_password)

   
scripts_dir = os.environ['DRINK_HOME'] + "/../vmanager_config/"
if debug == 1:
   print(scripts_dir)

#  Build request to add user action
headers = {
   'content-type':'application/json',
   'X-VMGR-Routing-Finalize' : 'true'
}

# 
request = {
   "name": "TSS Exp",
   "description": "Exports for_credit=True sessions",
   "scriptPath": scripts_dir + "/export_sessions_button.tcl",
   "iconPath": scripts_dir + "/export_icon_143019.png"
}

# 'post' the request to "/user-action/create" end point
response = post(url='/user-action/create', request=request, headers=headers)

# Check for valid response - exit if not
check_response(url='/user-action/create', response=response, exit_status=1, debug=0)


