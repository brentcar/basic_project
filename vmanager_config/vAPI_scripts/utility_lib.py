"""
  Low level Python functions to execute vAPI endpoints.  This is from vAPI
  Cookbook 
"""

import argparse
import sys
import os
import ssl
import requests
import json

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager

args = ""
rest_url = ""
authentication = ()

class MyAdapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections,
                                       maxsize=maxsize,
                                       block=block,
                                       ssl_version=ssl.PROTOCOL_TLSv1)
        
def set_server(project, server="", user="", passwd=""): 

    vmanager_server = server

    requests.packages.urllib3.disable_warnings()

    #Setup URL to the vManager server.
    global rest_url
    rest_url = "https://" + server + '/'+ project + '/vapi/rest'

    global authentication 
    authentication = (user,passwd)

def get_args():
    """
    Captures invocation arguments.
    e.g. get_args()
    """
    
    global args
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-server", help="server:port", required=True)
    parser.add_argument("-project", help="project name", required=True)
    parser.add_argument("-user", help="user name", required=True)
    parser.add_argument("-spectext", help="Text file containing spec changes", required=True)
    parser.add_argument("-vplan", help="vPlan to audit", required=True)
    parser.add_argument("-update_vplan",
                        help="When specified the vPlan will be updated with the changed spec text",
                        action='store_true',
                        default=False)
    parser.add_argument("-vplan_path",
                        help="vPlan hiearchy. defaults to the entire vPlan",
                        required=False,
                        default="")
    
    args = parser.parse_args()

    set_server(
        project=args.project,
        server=args.server,
        user=args.user,
        passwd="letmein")
    return args


def post(url, request, headers={'content-type':'application/json'}, description="", show_request=False,run_example=True,ignore_exceptions=False):
    
        if show_request:
            #print "###########################################################################"
            print("#--------------------------------------------------------------------------")
            print("Description:\t"+description)
            print("rest_url\t" + rest_url)
            print("url\t" + url)
            print("Command:\tpost " + rest_url + url)
            print("Headers:\t" + str(headers))
            print("Request:\t" + json.dumps(request, indent=4, sort_keys=False,separators=(",",":")))
            #print "###########################################################################"
            print("#--------------------------------------------------------------------------")            

            print()

        req = requests.Session()
        req.mount('https://', MyAdapter())

        data_json = json.dumps(request)

        if run_example:
            #print "running example"
            req = requests.Session()
            response = req.post(url=rest_url+url, data=data_json, headers=headers, auth=authentication, verify=False)
            
##            response = [{}]
##            try:
##                req = requests.Session()
##                response = req.post(rest_url+url,data=data_json,headers=headers,auth=authentication,verify=False)
##                response.raise_for_status()
##
##                
##            except requests.exceptions.RequestException:
##                if not ignore_exceptions:
##                    print "Failed: (" +response.reason + " : " +  str(response.status_code) +")"
##                    print "Failure reason:\n" + response.text + "\n"
##                    print "url:"+rest_url+url+"\n"
##                    print "request:"
##                    print json.dumps(request, indent=4, sort_keys=True,separators=(",",":"))
##                    sys.exit(1)

            return response
        else:
            reponse = None

def put(url,request,headers={'content-type':'application/json'},description="",show_request=False,run_example=True,ignore_exceptions=False):
        #new post used to handle python bug.
        
        if show_request:
            print("#--------------------------------------------------------------------------")
            print("Description:\t"+description)
            print("Command:\tpost " + rest_url + url)
            print("Headers:\t" + str(headers))
            print("Request:\t\n" + json.dumps(request, indent=4, sort_keys=False,separators=(",",":")))
            print("#--------------------------------------------------------------------------")            

            print()

        req = requests.Session()
        req.mount('https://', MyAdapter())

        data_json = json.dumps(request)

        if run_example:
            #print "running example"
            
            req = requests.Session()
            response = req.put(rest_url+url,data=data_json,headers=headers,auth=authentication,verify=False)
            return response
        else:
            return None



def get(url,description="",parameters="",show_request=False,run_example=True):
        headers={'content-type':'application/json'}
        get_url = rest_url+url+parameters

        if show_request:
            #print "###########################################################################"
            print("#--------------------------------------------------------------------------")            
            
            print("Description:\t"+description)
            print("Command:\tget " + get_url)
            print("Headers:\t" + str(headers))
            #print "###########################################################################"
            print("#--------------------------------------------------------------------------")            
            print()


        req = requests.Session()
        req.mount('https://', MyAdapter())

        if run_example:
            response = [{}]
            try:

                response = req.get(get_url,headers=headers,auth=authentication,verify=False)
            except requests.exceptions.RequestException:
                print("Failed: (" +response.reason + " : " +  str(response.status_code) +")")
                print("Failure reason:\n" + response.text + "\n")
                print("url:"+rest_url+url+parameters+"\n")
                sys.exit(1)

            return response
        

def put(url,request,headers={'content-type':'application/json'},description="",show_request=False,run_example=True,ignore_exceptions=False):
        #new post used to handle python bug.
        
        if show_request:
            print("#--------------------------------------------------------------------------")
            print("Description:\t"+description)
            print("Command:\tpost " + rest_url + url)
            print("Headers:\t" + str(headers))
            print("Request:\t\n" + json.dumps(request, indent=4, sort_keys=False,separators=(",",":")))
            print("#--------------------------------------------------------------------------")            

            print()

        req = requests.Session()
        req.mount('https://', MyAdapter())

        data_json = json.dumps(request)

        if run_example:
            #print "running example"
            
            req = requests.Session()
            response = req.put(rest_url+url,data=data_json,headers=headers,auth=authentication,verify=False)
            return response
        else:
            return None




def delete(url, request, headers={'content-type':'application/json'}, description="", show_request=False,run_example=True,ignore_exceptions=False):
    
        if show_request:
            #print "###########################################################################"
            print("#--------------------------------------------------------------------------")
            print("Description:\t"+description)
            print("rest_url\t" + rest_url)
            print("url\t" + url)
            print("Command:\tpost " + rest_url + url)
            print("Headers:\t" + str(headers))
            print("Request:\t" + json.dumps(request, indent=4, sort_keys=False,separators=(",",":")))
            #print "###########################################################################"
            print("#--------------------------------------------------------------------------")            

            print()

        req = requests.Session()
        req.mount('https://', MyAdapter())

        data_json = json.dumps(request)

        if run_example:
            #print "running example"
            req = requests.Session()
            response = req.delete(url=rest_url+url, data=data_json, headers=headers, auth=authentication, verify=False)
            
##            response = [{}]
##            try:
##                req = requests.Session()
##                response = req.post(rest_url+url,data=data_json,headers=headers,auth=authentication,verify=False)
##                response.raise_for_status()
##
##                
##            except requests.exceptions.RequestException:
##                if not ignore_exceptions:
##                    print "Failed: (" +response.reason + " : " +  str(response.status_code) +")"
##                    print "Failure reason:\n" + response.text + "\n"
##                    print "url:"+rest_url+url+"\n"
##                    print "request:"
##                    print json.dumps(request, indent=4, sort_keys=True,separators=(",",":"))
##                    sys.exit(1)

            return response
        else:
            reponse = None



