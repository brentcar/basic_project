"""
###########################################################################
####################  export_for_credit_sessions.py #######################
###########################################################################

compact_session_aad_export.py Python script has 1 optional argument: 
    exported_vsofx_dir_prefix   :  Prefix of directory to place exported vsofx 
                                   file of compacted session.  Directory name will
                                   include data/time in vManager session directory
                                   format to insure it is unique.
                                   If not specified "exported_sessions" will be assumed
                                   

For example:
  python3 export_for_credit_sessions.py \
  exported_vsofx_dir_prefix drink_machine
  
###########################################################################
"""

from utility_lib import *
from filter_lib import *

from datetime import *
import traceback
import time

import os
import random


#  Check response from vAPI commands
def check_response (url="", response=None, exit_status=1, debug=0) :
    """
    Parses vAPI response to determine pass/fail.  Prints formatted error 
    message and exits with non-zero exit status for failure.  If debug=1
    provide additional debug information if possible.

    Arguments:
      url         : vAPI endpoint (only used in error message)
      response    : response from vAPI command call
      exit_status : Exit status of Python script in case of failure
      debug       : Enables additional debug information when set to 1
    """
    if response == None :
      print("\nvAPI request: " + url + " failed! \n")
      print("Call stack is:")
      traceback.print_stack()
      sys.exit(exit_status)
       
    #  If not a passing status_code, print status_code, reason and exit with requested exit_status
    if response.status_code not in [200, 204, 206] :
      print("\nvAPI request: " + url + " failed with exit code: " + str(response.status_code) +  " !")
      print("Failure reason: " + response.text + "\n" )
      print("Call stack is:")
      traceback.print_stack()
      sys.exit(exit_status)

    if debug > 0 :
      try :
          foo = response.json()
      except :
         #  Not a json response
         print("\nResponse from " + url + ": " + str(response) + " is not a valid JSON response!\n")
         print("Call stack is:")
         traceback.print_stack()
         sys.exit(exit_status)
      else :
         #  We do have a json response
         response_list = response.json()
         print("\nDebug: response from " + url + ":")
         print(json.dumps(response_list, indent=4, sort_keys=True,separators = (",",":")))    



# Parse command-line arguments 
def export_for_credit_sessions_args():
    """
    Parses and captures invocation arguments returning
    parsed arguments

    Arguments : None
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-exported_vsofx_dir_prefix", help="Prefix of directory name to place exported vsofx file", required=False)
    args = parser.parse_args()
    return args

#  Get for_credit sessions in project
def get_for_credit_sessions(headers={'content-type':'application/json'}, debug=0):
  """
  Returns a list of sessions with for_credit set to true from the project

  Arguments:
    session_name : Name of target session
    headers      : Headers for vAPI endpoint call
    debug        : Enables additional debug information when set to 1

  """

  #get all for_credit sessions returning only session name
  request={ 

      "sortSpec" : [
                      {"attName": "name",
                       "direction" : "DESCENDING"
                      }
                   ],
      "pageOffset"   : 0,
      "pageLength" : 1000000,
      "filter"     : att_value_filter("EQUALS","for_credit",True),
      "projection" : { 
                        "type"      : "SELECTION_ONLY",
                        "selection" : ["name"]
                     }
  }

  #ask for runs
  response = post(url='/sessions/list', request=request, headers=headers)
  check_response(url='/sessions/list', response=response, exit_status=3, debug=debug)

  response_list = response.json()

  return response_list 


#  Export specified session
def export_session( session_name="", export_dir="", withSessionDir=False, headers={'content-type':'application/json'}, debug=0) :
   """
   Export specified session as vsofx file into specified export directory.
   Note:  Session directory is not included in vsofx file unless withSessionDir is set to True

   Arguments:
     session_name   : Name of session in project to export
     export_dir     : Directory to place vsofx file containing session
     withSessionDir : 
     headers        : headers for vAPI endpoint call
     debug          : Enables additional debug information when set to 1
   """

   # Build request
   request = {  
      "rs" : {
        "filter" : att_value_filter("EQUALS", "name", session_name)
      },  
      # TODO  This directory must be writable by the unix account that started the vManager server
      "topDir" : export_dir,
      "withSessionDir" : withSessionDir,
      "useSessionName" : True,
      "overwrite"      : True,
      "deleteSessionOption" : "NO_DELETION"
   }

   response = post(url='/sessions/export', request=request, headers=headers)
   check_response(url='/sessions/export', response=response, debug=debug )
 


#  Extract command-line argument
args = export_for_credit_sessions_args()
vmgr_server = "localhost:8080"
vmgr_project = "vmgr"
vmgr_username = os.environ["USER"]
vmgr_password = "letmein"

# If exported_vsofx_dir_prefix argument is not specified, "use export_sessions"
if args.exported_vsofx_dir_prefix == None :
  exported_vsofx_dir_prefix = "exported_sessions"
else :
  exported_vsofx_dir_prefix = args.exported_vsofx_dir_prefix


#  Build name for exported_vsofx directory in vManager nomenclature
current_time = time.localtime(time.time())
exported_vsofx_dir = os.environ['HOME'] + "/" + exported_vsofx_dir_prefix + "." + vmgr_username + "." + time.strftime("%y_%m_%d_%H_%M_%S", current_time) + "_" + str(random.randint(0,9999)).zfill(4)


# Connect to project on the server
set_server(project=vmgr_project, server=vmgr_server, user=vmgr_username, passwd=vmgr_password)

# headers for vAPI endpoint calls
headers = {
        'content-type':'application/json'
}

#  Get list of sessions with for_credit UDA set to true
session_list = get_for_credit_sessions(headers=headers) 

#  Exit if there are no marked for export
if len(session_list) == 0 :
  print("\nThere are no sessions with for_credit set to true to export! \n")
  sys.exit(0)
else :
  print(f"\nExporting {len(session_list)} sessions with for_credit set to true to {exported_vsofx_dir}")


for session in session_list :
  # Export vsofx files including session directory for each session
  export_session( session_name = session['name'], 
                  withSessionDir = True,
                  export_dir = exported_vsofx_dir,
                  headers = headers) 
  print(f"      Successfully exported {session['name']} session to {exported_vsofx_dir} ")

print(f"\nCopy {exported_vsofx_dir} directory to TSS repository and push TSS repository to be pulled by master vManager session\n")


