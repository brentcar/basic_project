"""
###########################################################################
#########################  import_sessions.py #######################
###########################################################################

import_sessions Python script imports all vsofx files found in the
import directory to the specified project.

import_sessions Python script requires 1 argument: 
    import_dir           :  Directory containing exported vsofx 
                            files to import
Note: import_dir directory must be readable by the Unix account 
of the vManager server or the vManager project account (if used).

For example:
  python3 import_sessions.py -import_dir /tmp/exported_vxofxs
  
###########################################################################
"""
from utility_lib import *

import os
import glob

# Parse input arguments #
def import_sessions_args():
    """
    Parses and captures invocation arguments returning
    parsed arguments

    Arguments : None
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-import_dir",  help="Directory of VSOFX files", required=True)
    args = parser.parse_args()
    return args


#  Import vsofx file to project
def import_vsofx(vsofx_file, topDir, headers={'content-type':'application/json'}, debug=0) :
  """
  Import specified vsofx file in current vManager project

  Arguments:
    vsofx_file       : Full unix path to the vsofx file to import
    headers          : Headers for vAPI endpoint call
    debug            : Enables additional debug information when set to 1
 
  """

  request = {
      "topDir"   : topDir,
      "vsofPath" : vsofx_file
  }

  response = post(url='/sessions/import', request=request, headers=headers)
  check_response(url='/sessions/import', response=response, debug=debug )


#  Check response from vAPI commands
def check_response (url="", response=None, exit_status=1, debug=0) :
    """
    Parses vAPI response to determine pass/fail.  Prints formatted error 
    message and exits with non-zero exit status for failure.  If debug=1
    provide additional debug information if possible.

    Arguments:
      url         : vAPI endpoint (only used in error message)
      response    : response from vAPI command call
      exit_status : Exit status of Python script in case of failure
      debug       : Enables additional debug information when set to 1
    """
    if response == None :
      print("\nvAPI request: " + url + " failed! \n")
      print("Call stack is:")
      traceback.print_stack()
      sys.exit(exit_status)
       
    #  If not a passing status_code, print status_code, reason and exit with requested exit_status
    if response.status_code not in [200, 204, 206] :
      print("\nvAPI request: " + url + " failed with exit code: " + str(response.status_code) +  " !")
      print("Failure reason: " + response.text + "\n" )
      print("Call stack is:")
      traceback.print_stack()
      sys.exit(exit_status)

    if debug > 0 :
      try :
          foo = response.json()
      except :
         #  Not a json response
         print("\nResponse from " + url + ": " + str(response) + " is not a valid JSON response!\n")
         print("Call stack is:")
         traceback.print_stack()
         sys.exit(exit_status)
      else :
         #  We do have a json response
         response_list = response.json()
         print("\nDebug: response from " + url + ":")
         print(json.dumps(response_list, indent=4, sort_keys=True,separators = (",",":")))    


def does_directory_exist(directory_path, debug=0)  :
   """
   Confirm directory_path is a fully specified path and exists. 

   Returns True if directory exists and False otherwise

   Arguments:
     directory_path : Full unix path to desired directory
     debug          : Enables additional debug information when set to 1
   """

   #  Confirm directory_path is an absolute and not relative path
   if not os.path.isabs(directory_path) :
     print("\nError! directory_path argument must be an absolute path.  Provided directory_path is " + directory_path + "\n")
     print("Call stack is:")
     traceback.print_stack()
     sys.exit(1)

   #  Confirm directory_path exists 
   if os.path.isdir(directory_path) :
     return True
   else :
     return False




#  Check that session exists on specified server and project
def does_session_exist (session_name, headers={'content-type':'application/json'}, debug=0) : 
  """
  Checks if session exists on server project.  Returns True or False

  Arguments:
    session_name : name of target session
    headers      : headers for vAPI command
    debug        : Enables additional debug information when set to 1

  """

  request = {  
     "pageLength" : 10000000,
     "projection" : { 
                      "selection" : ['name']
                    }
  }

  response = post(url='/sessions/list', request=request, headers=headers )
  check_response(url='/sessions/list', response=response, debug=debug )

  response_list = response.json()

  # Return True if session_name exists or return False
  for session in response_list :
    if session['name'] == session_name :
      return True

  return 



def create_directory(directory_path, debug=0)  :
   """
   Create directory with proper group permissions to insure vManager server can
   write into directory and if specfied directory exists that the proper group
   RWX permissions are set

   Arguments:
     directory_path : Full unix path to desired directory
     debug          : Enables additional debug information when set to 1
   """

   #  Confirm directory_path is an absolute and not relative path
   if not os.path.isabs(directory_path) :
     print("\nError! directory_path argument to create_directory must be an absolute path.  Provided directory_path is " + directory_path + "\n")
     print("Call stack is:")
     traceback.print_stack()
     sys.exit(1)

   #  Create directory_path if it does not exist and set permission on directory to 775 to enable 
   #  vAPI to write into directory.  
   if not os.path.isdir(directory_path) :
     if debug :
       print("Making directory with proper permissions : " + directory_path )
     try :
       os.makedirs(directory_path)
     except OSError:
       print("\nError! Cannot create " + directory_path + " !\n")
       print("Call stack is:")
       traceback.print_stack()
       sys.exit(1)

     # Set permission on directory_path to 774 to allow vAPI to write to directory
     os.chmod(directory_path, 0o774)
   else :
     #  directory_path exits - confirm that group permission is set to 7 
     if debug :
       print(directory_path + " already exists - updating permissions ")

     update_directory_unix_permissions(directory_path=directory_path, debug=debug)
 
   if debug :
     print("Directory " + directory_path + " ready for use") 


def update_directory_unix_permissions(directory_path, debug=0)  :
   """
   Confirm directory_path is a fully specified path, exists 
   If directory_path exists, update permissions enabling group RWX permissions.

   Returns True if directory exists and permissions were properly set

   Arguments:
     directory_path : Full unix path to desired directory
     debug          : Enables additional debug information when set to 1
   """

   #  Confirm directory_path exists and is a fully specified path
   if not does_directory_exist(directory_path) :
     print("\nError! " + directory_path + " does not exist!\n")
     print("Call stack is:")
     traceback.print_stack()
     sys.exit(1)
   
   #  Directory_path exits - confirm that group permission is set to 7 
   dir_stat = os.stat(directory_path)
   dir_group_perm = oct(dir_stat.st_mode)[5]
   if debug :
     print("Group Directory permission on: " + directory_path + " is: " + dir_group_perm)
  
   #  If group permission on directory_path is not 7, attempt to change permission to 7
   if (dir_group_perm != "7") :
     if debug :
       print("Attempting to reset group directory permission on: " + directory_path )
     try :
       os.chmod(directory_path, 0o775)
     except OSError:
       print("\nError! Cannot change permission on " + directory_path + " to 775!\n")
       print("Call stack is:")
       traceback.print_stack()
       sys.exit(1)


#  Check that session exists on specified server and project
def does_session_exist (session_name, headers={'content-type':'application/json'}, debug=0) : 
  """
  Checks if session exists on server project.  Returns True or False

  Arguments:
    session_name : name of target session
    headers      : headers for vAPI command
    debug        : Enables additional debug information when set to 1

  """

  request = {  
     "pageLength" : 10000000,
     "projection" : { 
                      "selection" : ['name']
                    }
  }

  response = post(url='/sessions/list', request=request, headers=headers )
  check_response(url='/sessions/list', response=response, debug=debug )

  response_list = response.json()

  # Return True if session_name exists or return False
  for session in response_list :
    if session['name'] == session_name :
      return True

  return False

 
 
#  Extract command-line arguments
args = import_sessions_args()
vmgr_server = "localhost:8080"
vmgr_project = "vmgr"
vmgr_username = os.environ['USER']
vmgr_password = "letmein"
import_vsofx_dir = args.import_dir

#  Create directory for imported sessions if it does not already exist

imported_sessions_dir = os.environ["HOME"] + "/imported_sessions_dir"
create_directory(imported_sessions_dir)


# Note: The import_vsofx_dir directory must be readable by the Unix account of the vManager server
#       or the vManager project account (if used).

# Strip trailing '/' from import_vsofx_dir if specified 
import_vsofx_dir = import_vsofx_dir.rstrip('/')

#connect to the server
set_server(project=vmgr_project, server=vmgr_server, user=vmgr_username, passwd=vmgr_password)

# headers for vAPI endpoint calls
headers = {
        'content-type':'application/json'
}

# Check that specified import directory exists
if not os.path.isdir(import_vsofx_dir) :
  print("\nSpecified import directory: " + import_vsofx_dir + " does not exist!\n")
  sys.exit(1)

#  Get list of vsofx files in import_vsofx_dir.  
#  Note: Will be full unix path to vsofx files
vsofx_file_list = glob.glob(import_vsofx_dir + "/*.vsofx")

# Confirm there is at least one vsofx file is in import directory
if len(vsofx_file_list) == 0 :
  print("\nNo vsofx files found in import directory: " + import_vsofx_dir + "\n")
  sys.exit(1)

print("\nImporting following vsofx files into " + vmgr_project + " project")

#  Import all vsofx files in import directory skipping previously imported vsofx files
for vsofx_file in vsofx_file_list :

  # Extract session_name from full unix path to vsofx file by removing unix path 
  # and .vsofx file extension
  session_name = vsofx_file.replace(import_vsofx_dir + "/","")
  session_name = session_name.replace(".vsofx","")

  # Do not import vsofx file if session exists from previous import of vsofx file
  if does_session_exist(session_name=session_name, headers=headers) :
    print("   Skipping already imported " + vsofx_file)
  else :
    print("   Importing " + vsofx_file)
    import_vsofx(vsofx_file=vsofx_file, topDir=imported_sessions_dir, headers=headers) 



