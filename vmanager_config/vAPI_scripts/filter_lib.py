"""
  Python functions to simplify writing vAPI filters.  This is from vAPI
  Cookbook 
"""

def in_filter(operand,attname,values):
    """
    Create an IN filter
    operand values: IN | NOT_IN
    e.g. in_filter("IN","status","passed"),
    """

    filter={
        "@c":".InFilter",
        "operand":operand,
        "attName":attname,
        "values":values
    }
    return filter

def att_value_filter(operand,attname,attvalue):
    """
    Create an Attribute value filter
    operand values: EQUALS | NOT_EQUALS | GREATER_THAN | GREATER_OR_EQUALS_TO | LESS_THAN | LESS_OR_EQUALS_TO
    e.g. att_value_filter("EQUALS","id",44)
    """

    filter={
        "@c": ".AttValueFilter",
        "operand": operand,
        "attName": attname,
        "attValue":  attvalue,
    }
    return filter

def expression_filter(attname,expression,shouldmatch=True,posixmode=False):
    """
    Create an expression filter
    "shouldMatch": shouldmatch,
    "posixMode": posixmode
    Note: Default value of shouldMatch is true. If set to false, the expression will be negative.
    Note: Default value of posixMode is false. If set to true, the expression will be treated as a standard regular expression.

    e.g. expression_filter("parent_session_name","vm_uart*") 
    """
    
    filter={
        "@c": ".ExpressionFilter",
        "attName": attname,
        "exp":  expression,
        "shouldMatch": shouldmatch,
        "posixMode": posixmode
    }
    return filter

def chained_filter(condition,chain):
    """
    Create a chained filter.
    e.g. 
    chained_filter("AND",[
                in_filter("IN","status",["passed"]),
                expression_filter("parent_session_name",session_exp)
                ]
    """

    filter={
        "@c":".ChainedFilter",
        "condition":condition,
        "chain": chain
    }
    return filter

def relation_filter(relationname, filter):
    """
    Create a relation filter.
    e.g.
    relation_filter(relationname="session", relationfilter=expression_filter(attname="session_name", expression="vm_uart_*_2497")),    
    """
    
    new_filter = {
        "@c":".RelationFilter",
        "relationName":relationname,
        "filter": filter
    }
    return new_filter








         

