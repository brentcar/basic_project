#!/bin/bash

echo "(INFO) - Checking for configured vManager root installation...               "
export VMGR_ROOT_INSTALLATION=`vm_root`

if [[ -z ${VMGR_ROOT_INSTALLATION} ]]; then
    echo "(ERROR) - There was no PATH to vManager executables found. Please set the PATH variable to include vManager's executables."
    exit 1
fi
echo "(INFO) - Using vManager installation ${VMGR_ROOT_INSTALLATION}"


#################################################################################################################################################################
## Parsing the command-line arguments
while [[ $# -gt 0 ]]; do
    key="${1}"
    
    case ${key} in
        -h|--help)
            echo "(INFO) import_sessions.sh options are:"
            echo "(INFO)    -h | --help                              : Prints this help text"
            echo "(INFO)"
            echo "(INFO)   Mandatory options. If not specified, an error will be issued."
            echo "(INFO)    -import_dir <DIR>                 : Must provide the directory containing VSOFX files to be imported"
            echo "(INFO)                                        into server"
            echo "(INFO)  Example:"
            echo "(INFO)    import_sessions.sh -import_dir /tmp/exported_sessions_dir"
            shift # remove single argument
            exit 0
            ;;
        -import_dir)
            shift # remove the -inst_root argument
            export VMGR_SESSION_IMPORT_DIR=${1}
            shift # remove the argument
            echo "(INFO) - Importing sessions from directory :  ${VMGR_SESSION_IMPORT_DIR}"
            ;;
       *)  # default is also unknown option
            echo "(WARNING) - Unknown argument ${key} found. Please remove it from the script and run again"
            exit 1
            ;;
    esac
done


###############################################################################################################################################
## These Parameters must be given by the user as a bare minimum
if [[ -z "${VMGR_SESSION_IMPORT_DIR}" ]]; then
    echo "(ERROR) - Option -import_dir <DIR> not provided. Please provide the directory containing VSOFX files to import"
    exit 1
fi

# Confirm $DRINK_HOME environment variable is defined.
if [[ -z ${DRINK_HOME} ]]; then
  echo "(ERROR) - DRINK_HOME environment variable pointing to root of drink machine testcase is not defined"
  exit 1
fi

#  Use $DRINK_HOME environment variable to locate import_sessions.py vAPI script to execute
#  and leverage python3 executable within `vm_root`

$VMGR_ROOT_INSTALLATION/tools/python*/bin/python3 $DRINK_HOME/../vmanager_config/vAPI_scripts/import_sessions.py -import_dir $VMGR_SESSION_IMPORT_DIR


