#  Setup env variable pointing to top of design
setenv DRINK_HOME $PWD

#  Define alias for vmgr loading configuration preferences

alias vmgr 'vmanager -regr -server localhost:8080 -init $DRINK_HOME/../vmanager_config/user_config.tcl'


