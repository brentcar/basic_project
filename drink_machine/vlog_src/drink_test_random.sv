`timescale 1ns/ 10 ps
module test_drink_random;

wire  dime_out, dispense, empty, nickel_dime_out, nickel_out, two_dime_out;
     
reg  clk, nickel_in, dime_in, quarter_in, load, reset;

reg  [7:0] r_cans, r_nickels, r_dimes;

bit [3:0] iteration_count;
int error_inject;

drink_machine_top top(
	.clk(clk),.nickel_in(nickel_in), .dime_in(dime_in), 
	.quarter_in(quarter_in), .load(load),  .reset(reset),
	.cans(r_cans), .nickels(r_nickels), .dimes(r_dimes),
	.nickel_out(nickel_out), .dime_out(dime_out), 
	.two_dime_out(two_dime_out), .dispense(dispense), 
	.empty(empty), .exact_change(exact_change)
); 


	initial
		begin : generate_clock
			clk = 0;
                        r_nickels = 0;
                        r_cans = 0;
                        r_dimes = 0;
			forever #100 clk = ~clk;
		end

  initial
    begin
      repeat (10) @(posedge clk);
      assert (randomize(iteration_count ) with {iteration_count > 0;}); 
      $display("Start applying random stimuli");
      repeat(iteration_count) 
         begin
            repeat (10) @(posedge clk);
            assert (randomize(r_cans, r_nickels, r_dimes) with {r_cans != 0; 
                                                                r_cans <= 50; 
                                                                r_nickels != 0; 
                                                                r_nickels <= 50; 
                                                                r_dimes != 0;
                                                                r_dimes <= 50; 
                                                                })
      
            $display("r_cans = %d  r_nickels = %d  r_dimes = %d", r_cans, r_nickels, r_dimes);
            reset = 1;
            repeat (2) @(posedge clk);
            reset = 0;
            repeat (2) @(posedge clk);
            repeat (2) @(posedge clk);
            load = 1;
            @(posedge clk);
            load = 0;
            repeat (2) @(posedge clk);
            repeat (1000)
              begin
                assert (randomize(quarter_in, dime_in, nickel_in));
                @(posedge clk);
              end
            void'(randomize(error_inject));
      
            //  Mark 1/10 of tests as failures
            if ((error_inject % 10) != 1)
               $display(">>>>> PASSED <<<<<");
            else
               $display(">>>>> FAILED <<<<<");
         end

      $finish;
    end

endmodule
