// Verilog HDL for gdglib, can_counter _functional

`timescale 1 ns / 100 ps
module can_counter( clk, load, count, dispense, empty);

	input       clk, load, dispense;
	input [7:0] count;
	output      empty;

	reg [7:0]   left; 

	wire        empty = !(|left);


//  Cover refill of drink machine
// psl cover_refill : cover {left==0; left > 0} @(posedge clk);

// Cover empty
/*
   psl cover_empty : cover {empty} @(posedge clk);

//  Cover Dispense
   psl cover_dispense : cover {dispense} @(posedge clk);

// never empty after load
   psl empty_after_load : assert always (load -> next !empty) @(posedge clk);

// Never assert empty without dispense
// // psl empty_without_dispense : assert always (empty -> dispense) @(posedge clk);
   psl empty_without_dispense : assert always (rose((left==0)) -> (empty & dispense)) @(posedge clk);

// Never load of 0 cans
   psl load_of_zero_cans : assume always (load -> (count>0)) @(posedge clk);
*/



  always @(negedge clk)
    if (load && !dispense)
      left <= count;
    else if(!load && dispense)
      left <= left - 1;

  covergroup can_counter_cg @(posedge clk);
    dispense_cp : coverpoint dispense;
    empty_cp : coverpoint empty;
    load_cp : coverpoint load;
  endgroup

  can_counter_cg can_counter = new();

  covergroup cans_cg @(posedge clk);
    cans_left_cp : coverpoint left;
  endgroup

  cans_cg cans = new();

endmodule
