#  Remove hostname from prompt
set prompt="%%[%n %c]% "

#  Setup vManager
setenv VMGR /tools/cds/vmanagermain/20.03.001
set path = ($VMGR/tools/bin $path)

#  Setup Xcelium
setenv XLM  /tools/cds/xcelium/19.09.010
set path = ($XLM/tools/bin $XLM/tools/cdsgcc/gcc/bin/64bit $path)

#  Setup VXE
setenv VXE /tools/cds/vxe/19.10.185.s005
set path = ($VXE/tools/bin $path)

#  Setup Cadence AE license server
setenv CDS_LIC_FILE 5280@cadence-82914857-cadencelm.trustedstratus.org

echo " "
echo "Using vManager: $VMGR"
echo "Using Xcelium: $XLM"
echo "Using VXE: $VXE"
echo "Using CDS_LIC_FILE : $CDS_LIC_FILE"
echo " "

echo "Setting up drink_machine"
pushd drink_machine
source setup_drink_machine.csh
popd

