================================================
Pulling example from GitLab
================================================
    % cd $HOME
    % git init basic_project
    % cd basic_project
    % git pull -u https://gitlab.com/brentcar/basic_project.git master

================================================
Setup a new terminal
================================================
- Setup Unix search path and licensing - tcsh based

    % tcsh
    % cd $HOME/basic_project
    % source setup.csh


================================================
Starting and configuring a new vManager server 
   vManager server port: 8080
   Postgress database : 5432
================================================

- Open a terminal and configure as described above if needed.

- Start a new vManager server for project on new TSS instance using 8080 for vManager 
  server port and 5432 as Posgress database port.  

  Note: Do this only once on each new TSS instance.  

    % $DRINK_HOME/../vmanager_config/vmanager_server_setup/vmanager_server_setup 



====================================================================
Start, stop, restart or query status on an existing vManager server 
====================================================================

- If needed, open a terminal and configure as described above.

- Perform desired server operation

    % cd $DRINK_HOME/..
    % ./vmanager_service_20.03-s001 {start|stop|restart|status}


=============================================================
Running regressions in vManager
=============================================================

- Open a terminal and configure as described above if needed.

- Launch desired regression.  Note: vmgr is a shell alias to reference the 
  vManager server and initialize the project environment.

    % cd $DRINK_HOME/vsifs
    % vmgr -execcmd "launch weekly_regression.vsif"

  Note:  If you wish to launch the session and monitor the progress of the session 
  in vManager:
    % vmgr -launch weekly_regression.vsif

          
  When the session completes, the post-session automation script will generate
  an HTML report in $DRINK_HOME/session_report.html

==============================================================
Exporting for_credit vManager sessions from this TSS instance 
for import into Master vManager session
==============================================================

  - Open a terminal and configure as described above if needed

  - Launch vManager
        % vmgr &

  - Click "Upgrade to full client" icon in uppper left hand toolbar if not 
    already using the full vManager client license.

  - In vManager Regression, select desired sessions to export.
    
  - From RMB popup menu, select "Edit all at once" and in "Edit all at once" 
    form, set "For Credit" field to True and click OK.

    Note: This sets the for_credit User Defined Attribute (UDA) on the selected sessions to True

  - If the "Scripts Manager" and "TSS Exp" icons are not shown in the left side of the vManager client, 
    click the orange "Star" icon on the far right side of the vManager Client.

  - Click "TSS Exp" icon and follow the directions echoed to the terminal where vManager was launched.  
    
    Note:  Copy the exported session directory to the TSS repository and push the TSS repository for 
    the master vManager session to pull.

     
=================================================================================
Importing vManager sessions from other TSS instances on Master vManager server
=================================================================================

  - Go to TSS instance containing master vManager server. 

  - Open a terminal and configure as described above if needed

  - Pull TSS repository containing exported sessions pushed from slave TSS instance

  - $DRINK_HOME/../vmanager_config/import_sessions.sh -import_dir <DIR>

       where <DIR> is path to the directory on the repository containing the exported
       sessions.

    IMPORTANT:  The session directories associated with all imported vManager sessions are 
    placed inside $HOME/imported_sessions_dir.  Do *NOT* delete this directory!

